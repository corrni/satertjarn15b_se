# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# to seed the rent_periods table
require 'csv'
require 'json'

rent_periods = CSV.parse(File.read "#{Rails.root}/db/periods.csv")

def week_dates( week_num, year )
  week_start = Date.commercial( year, week_num, 1 )
  week_end = Date.commercial( year, week_num, 7 )
  d = { :range => week_start.strftime( "%m/%d/%y" ) + ' - ' + week_end.strftime( "%m/%d/%y" ),
        :start => week_start, 
        :end   => week_end }
end

def convert_csv(data)
  resulting_periods = data.map do |row|
    amt_week = row[1]
    amt_short_week = row[2]
    amt_weekend = row[3]    
    year = ( row[0].to_i < 20 ) ? 2013 : 2012
    d_range = week_dates(row[0].to_i, year)
        
    result = { :start => d_range[:start],
               :expires   => d_range[:end],
               :amt_week => amt_week,
               :amt_short_week => amt_short_week,
               :amt_weekend => amt_weekend,
               :enabled => true }
  end  
end

RentPeriod.create( convert_csv(rent_periods) )