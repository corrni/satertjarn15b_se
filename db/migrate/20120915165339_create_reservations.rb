class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :user_id
      t.integer :rent_period_id
      t.date :arrival
      t.date :departure
      t.boolean :confirmed
      t.string :reference

      t.timestamps
    end
  end
end
