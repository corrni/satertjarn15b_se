class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :name
      t.text :body
      t.string :title
      t.string :meta_keywords
      t.string :meta_description
      t.string :slug

      t.timestamps
    end
  end
end
