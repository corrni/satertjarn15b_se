# NOTE: SimpleEnum module requires a table column with the format
# 		{enum_type}_cd, hence reservation_type_cd

class ChangeReservationTypeOnReservations < ActiveRecord::Migration
	change_table :reservations do |t|
		t.rename :reservation_type, :reservation_type_cd
	end
end
