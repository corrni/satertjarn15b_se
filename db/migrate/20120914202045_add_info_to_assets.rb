class AddInfoToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :image, :string
    add_column :assets, :caption, :string
  end
end
