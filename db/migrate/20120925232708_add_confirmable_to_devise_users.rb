# NOTE: the "gapps_id" string is only necessary if individual users are going to 
# 		connect their account to Google's services
class AddConfirmableToDeviseUsers < ActiveRecord::Migration
  def change

  	## Confirmable
  	add_column :users, :confirmation_token, :string
  	add_column :users, :confirmed_at, :datetime
  	add_column :users, :confirmation_sent_at, :datetime
  	add_column :users, :unconfirmed_email, :string
  end
end
