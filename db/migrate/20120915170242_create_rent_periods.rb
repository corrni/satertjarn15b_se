class CreateRentPeriods < ActiveRecord::Migration
  def change
    create_table :rent_periods do |t|
      t.date :start
      t.date :expires
      t.integer :amt_week
      t.integer :amt_short_week
      t.integer :amt_weekend
      t.boolean :enabled

      t.timestamps
    end
  end
end
