class AddPriceToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :price, :integer
    remove_column :reservations, :reservation_type_cd
    add_column :reservations, :reservation_type, :string
  end
end
