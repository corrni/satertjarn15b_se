Satertjarn15bSe::Application.configure do
  config.cache_classes = true

  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  config.serve_static_assets = false
  config.assets.compress = true
  config.assets.compile = false
  config.assets.digest = true

  # hell yeah...
  config.assets.css_compressor = :yui
  config.assets.js_compressor = :yui
  
  config.assets.precompile += %w( polyfills.js )
  config.assets.precompile += %w( active_admin.js active_admin.css )
  config.assets.precompile += %w( bookings.js bookings.css )

    # Defaults to nil and saved in location specified by config.assets.prefix
  # config.assets.manifest = YOUR_PATH


  # Specifies the header that your server uses for sending files
  config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # See everything in the log (default is :info)
  # config.log_level = :debug

  # Prepend all log lines with the following tags
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)


  config.cache_store = :mem_cache_store, "localhost"


  # Disable delivery errors, bad email addresses will be ignored
  config.action_mailer.raise_delivery_errors = false


  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  config.active_support.deprecation = :notify
  config.active_record.auto_explain_threshold_in_seconds = 0.5


  # ssl config (rack-ssl-enforcer)
  config.middleware.use Rack::SslEnforcer, only: [ %r{^/admin/}, %r{^/users/} ]


  # NOTE: for some reason, the locale doesn't "stick" in production
  config.i18n.default_locale = "sv-SE"

  config.action_mailer.delivery_method = :simple_postmark
  config.action_mailer.simple_postmark_settings = { api_key: '65c52651-928f-4d8a-bd39-def9f7fbaa14' }
  config.action_mailer.default_url_options = { :host => "satertjarn15b.se" }


end
