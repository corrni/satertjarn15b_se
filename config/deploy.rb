# adapted from stuff found on 
# 	- https://github.com/wayneeseguin/rvm-capistrano

require "rvm/capistrano"
require "bundler/capistrano"



set :rvm_ruby_string, ENV['GEM_HOME'].gsub(/.*\//,"")
set :rvm_install_ruby_params, '--1.9'      # for jruby/rbx default to 1.9 mode

# set rvm when first rolling things out
before 'deploy:setup', 'rvm:install_rvm'   # install RVM
before 'deploy:setup', 'rvm:install_ruby'  # install Ruby and create gemset, or:
before 'deploy:setup', 'rvm:create_gemset' # only create gemset


set :application, "satertjarn15b_se"

set :scm,             :git
default_run_options[:pty] = true
set :repository,      "git@github.com:corrni/satertjarn15b_se.git"
set :branch,          "master"
set :migrate_target,  :current
ssh_options[:forward_agent] = true
set :deploy_via, :remote_cache

set :rails_env,       "production"

set :deploy_to,       "/home/ubuntu/apps/satertjarn15b_se"
set :normalize_asset_timestamps, false

set :location, "ec2-54-247-74-4.eu-west-1.compute.amazonaws.com"
role :web, location
role :app, location
role :db, location, :primary => true

set :user,            "ubuntu"
set :group,           "ubuntu"
set :use_sudo, false

ssh_options[:keys] = [File.join(ENV["HOME"], "Documents/work/code/deploy/credentials", "corrni.pem")]



namespace :deploy do

	desc "reload the database with seed data"
	task :seed do
    	run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  	end


  	# unicorn-related tasks
	desc "Start unicorn"
	task :start, :except => { :no_release => true } do
		run "cd #{current_path} ; bundle exec unicorn_rails -c config/unicorn.rb -D"
	end

	desc "Stop unicorn"
	task :stop, :except => { :no_release => true } do
		run "kill -s QUIT `cat /tmp/unicorn.satertjarn15b_se.pid`"
	end

	desc "Restart unicorn"
	task :restart, :except => { :no_release => true } do
		run "kill -s QUIT `cat /tmp/unicorn.satertjarn15b_se.pid`"
		run "cd #{current_path} ; bundle exec unicorn_rails -c config/unicorn.rb -D"
	end
	
  # TODO: figure out a way to speed up the precompilation step
end

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"
