Satertjarn15bSe::Application.routes.draw do

  match "bookings/index"

  devise_for :users

  resources :reservations


  mount Rich::Engine => '/rich', :as => 'rich'


  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config


  # for simpleviewer
  match '/gallery.xml' => "pages#gallery"


  # for the CMS
  match '/' 	 => "pages#show"
  match '/:slug' => "pages#show"
end
