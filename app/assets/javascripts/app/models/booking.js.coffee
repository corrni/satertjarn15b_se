class App.Booking extends Spine.Model
  @configure 'Booking', 'id', 'user_id', 'rent_period_id', 'price', 'arrival', 'departure', 'confirmed', 'reference', 'reservation_type'
  @extend Spine.Model.Ajax

  @url: "/reservations"