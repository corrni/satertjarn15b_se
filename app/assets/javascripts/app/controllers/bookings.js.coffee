$ = jQuery.sub()
Booking = App.Booking

$.fn.item = ->
  elementID   = $(@).data('id')
  elementID or= $(@).parents('[data-id]').data('id')
  Booking.find(elementID)

class New extends Spine.Controller
  events:
    'click [data-type=back]': 'back'
    'submit form': 'submit'
    
  constructor: ->
    super
    @active @render
    
  render: ->
    @html @view('bookings/new')

  back: ->
    @navigate '/bookings'

  submit: (e) ->
    e.preventDefault()
    booking = Booking.fromForm(e.target).save()
    @navigate '/bookings', booking.id if booking

class Edit extends Spine.Controller
  events:
    'click [data-type=back]': 'back'
    'submit form': 'submit'
  
  constructor: ->
    super
    @active (params) ->
      @change(params.id)
      
  change: (id) ->
    @item = Booking.find(id)
    @render()
    
  render: ->
    @html @view('bookings/edit')(@item)

  back: ->
    @navigate '/bookings'

  submit: (e) ->
    e.preventDefault()
    @item.fromForm(e.target).save()
    @navigate '/bookings'

class Show extends Spine.Controller
  events:
    'click [data-type=edit]': 'edit'
    'click [data-type=back]': 'back'
    'click [data-type=destroy]': 'destroy'

  constructor: ->
    super
    @active (params) ->
      @change(params.id)

  change: (id) ->
    @item = Booking.find(id)
    @render()

  render: ->
    @html @view('bookings/show')(@item)

    helper:
      format: (date) ->
        Date.parse(date).toDateString()

  edit: ->
    @navigate '/bookings', @item.id, 'edit'

  back: ->
    @navigate '/bookings'

  destroy: (e) ->
    item = $(e.target).item()
    item.destroy() if confirm('Sure?')



class Index extends Spine.Controller
  events:
    'click [data-type=edit]':    'edit'
    'click [data-type=destroy]': 'destroy'
    'click [data-type=show]':    'show'
    'click [data-type=new]':     'new'

  constructor: ->
    super
    Booking.fetch()
    Booking.bind 'refresh change', @render
    
    
  render: =>
    bookings = Booking.all()
    @html @view('bookings/index')(bookings: bookings)
    
  edit: (e) ->
    item = $(e.target).item()
    @navigate '/bookings', item.id, 'edit'
    
  destroy: (e) ->
    item = $(e.target).item()
    item.destroy() if confirm('Sure?')
    
  show: (e) ->
    item = $(e.target).item()
    @navigate '/bookings', item.id
    
  new: ->
    @navigate '/bookings/new'
  


  
class App.Bookings extends Spine.Stack
  controllers:
    index: Index
    edit:  Edit
    show:  Show
    new:   New
    
  routes:
    '/bookings/new':      'new'
    '/bookings/:id/edit': 'edit'
    '/bookings/:id':      'show'
    '/bookings':          'index'
    
  default: 'index'
  className: 'stack bookings'

