#= require json2
## jQuery is already loaded
#= require spine
#= require spine/manager
#= require spine/ajax
#= require spine/route

#= require_tree ./lib
#= require_self
#= require_tree ./models
#= require_tree ./controllers
#= require_tree ./views

class App extends Spine.Controller
  constructor: ->
    super
    @append(@bookings = new App.Bookings)
    
    Spine.Route.setup()    



# App is now on the global namespace
window.App = App