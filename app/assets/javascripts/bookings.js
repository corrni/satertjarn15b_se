//= require fullcalendar
//= require calendar

//= require spine
//= require app
//= require jquery-rest


// initiatite the spine app
jQuery(function() {
	new App({el: $("#bookings_app")});
});