$(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  
  $('#calendar').fullCalendar({

    // translate calendar UI to swedish
    buttonText: {
      today: 'idag',
      month: 'månad',
      week:  'vecka',
      day:   'dag'
    },
    dayNames: [ 'Söndag', 'Måndag', 'Tisdag', 
                'Onsdag', 'Torsdag', 'Fredag', 
                'Lördag'],
    dayNamesShort: [ 'Sön', 'Mån', 'Tis', 
                     'Ons', 'Tor', 'Fre', 
                     'Lör'],

    monthNames: [ 'Januari', 'Februari', 'Mars', 
                  'April', 'Maj', 'Juni', 'Juli',
                  'Augusti', 'September', 'Oktober', 
                  'November', 'December'],
    monthNamesShort: [ 'Jan', 'Feb', 'Mar', 
                       'Apr', 'Maj', 'Jun',
                       'Jul', 'Aug', 'Sep', 
                       'Okt', 'Nov', 'Dec' ],


    editable: true,  

    header: { left: '', 
              center: 'title',
              right: 'today prev,next' },

    defaultView: 'month',
    height: 600,
    slotMinutes: 15,
    
    loading: function(bool){
        if (bool) 
            $('#loading').show();
        else 
            $('#loading').hide();
    },
    
    // a future calendar might have many sources.        
    eventSources: [{
        url: '/reservations',
        color: 'lightblue',
        textColor: 'darkgray',
        ignoreTimezone: false
    }],        
    
    timeFormat: 'h:mm t{ - h:mm t} ',

    dragOpacity: "0.5",
    

    //http://arshaw.com/fullcalendar/docs/event_ui/eventDrop/
    eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc){
        updateReservation(event);
    },

    // http://arshaw.com/fullcalendar/docs/event_ui/eventResize/
    eventResize: function(event, dayDelta, minuteDelta, revertFunc){
        updateReservation(event);
    },

    // http://arshaw.com/fullcalendar/docs/mouse/eventClick/
    eventClick: function(event, jsEvent, view){
      $("#cal_modal").reveal();

      
    },


    dayClick: function(date, allDay, jsEvent, view, RentPeriod) {
      var day = date.getDay();
      if ((day != 0) && (day != 4)) {
        alert("Var god och välj Söndag eller Torsdag");
      } else {

      // trigger the (spine) new action first
      $("div.stack > div.active [data-type=new]").click();

      var modal_window = $('#cal_modal');
      var modal_window_submit = $("#cal_modal_submit");

      var arrival_box = $('#arrival');
      var departure_box = $('#departure');
      var rent_period_select = $("#rent_period_select");
      var price_display = $("#cal_modal_price");


      arrival_box.val(date.toDateString());  // set the arrival date on the arrival date input box

      fetch_available_periods(date);         // check for the applicable rent_period


      // get Event object (has data necessary to make reservations)
      var eventObj = $("#calendar").fullCalendar("clientEvents");  
      console.log(eventObj);


      var departure_date = date;                                                               


      // clear price, and departure dates
      price_display.text("");
      departure_box.val("");


      rent_period_select.foundationCustomForms();  // enable foundation custom select

      // // hide all the rent_period select options
      // rent_period_select.find('option').hide();

      // // if the day selected is a 




      // reveal the booking box
      modal_window.reveal({
        animation: 'fadeAndPop',
        closeOnBackgroundClick: true,
        dismissModalClass: 'close-reveal-modal'
      });


      rent_period_select.change(function(e) {        
        // set the current price into the Reservation object
        Reservation.price = Reservation.rent_period[$(e.currentTarget).val()].price

        var departure_date = 
                  new Date( date.getTime() + 
                            toDays(Reservation.rent_period[$(e.currentTarget).val()].stay));
        // display the price
        price_display.text( Reservation.price + " kr" );

        // display the departure date
        departure_box.val(departure_date.toDateString());
      });


      modal_window_submit.click(function(e) { 
        // gather the data necessary to create a reservation
        var reservation = {};

        reservation.arrival           = arrival_box.val()
        reservation.departure         = departure_box.val()
        reservation.rent_period_id    = Reservation.rent_period.id;
        reservation.user_id           = Reservation.user.id;
        reservation.reservation_type  = rent_period_select.val();
        reservation.price             = Reservation.price;

        console.log("the reservation object contains:");
        console.log(reservation);


        createReservation(reservation);
      });
      }


    },


  });
});

