// converts Number to days (in milliseconds)
function toDays(num) {return num * 1000*60*60*24 };



function fetch_available_periods(arrival) {
  
    // check the available rent_period, and prices        
    $.read( "/reservations/new", 

            { arrival: arrival },

            function (response) {

              // capture pricing info
              Reservation.rent_period["week"].price = response.amt_week;
              Reservation.rent_period["short_week"].price = response.amt_short_week;
              Reservation.rent_period["weekend"].price = response.amt_weekend;
              Reservation.rent_period.id = response.rent_period_id;
              Reservation.user.id = response.user_id;
            },
            function (error) { console.log("something went wrong!"); console.error(error); }
    );
};



function createReservation(reservation) {

  console.log("in createReservation...");
  console.log(reservation);

  // ajax call to store event in DB               
  $.create( "/reservations",
      { reservation: {  arrival: "" + reservation.arrival,
                        departure: "" + reservation.departure,
                        rent_period_id: reservation.rent_period_id,
                        user_id: reservation.user_id,
                        reservation_type: reservation.reservation_type,
                        price: reservation.price
                  }
    },
    
    function (response) { 
      
      // pop up the confirmation window for the reservation
      $("#booking_info p#reference").text(response.reference);

      console.log('successfully updated task');
      console.log(response);

      $("div#new_booking").fadeOut("fast");
      $("div#booking_info").fadeIn("slow");

      $("#calendar").fullCalendar("refectchEvents");
    },
    
    function (error) { console.log("something went wrong!"); console.error(error); }
  );

};




function updateReservation(reservation) {
    $.update( "/reservations/" + reservation.id,
              { reservation: 
                { reference:      reservation.reference,
                  arrival:        "" + reservation.start,
                  departure:      "" + reservation.end,
                  rent_period_id: reservation.rent_period_id,
                  user_id:        reservation.user_id }
              },
              function (response) { console.log('successfully updated task. ' + response); }
    );
};


