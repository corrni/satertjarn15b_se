//= require jquery
//= require jquery_ujs

//= require jquery-ui
//= require jquery-rest

//= require h5bp

//= require foundation
//= require foundation/app

//= require rails.validations

$(document).foundationAlerts(); 	// enable foundation alert boxes




// set up global reservation object
// TODO: you should move most of the behavior/data related to individual bookings here
// 		 (more maintainable, instead of fetching data from all over the place)
window.Reservation = {};
Reservation.rent_period = {
  id: "",
  week:       { "stay": 7, "price": "" },
  short_week: { "stay": 5, "price": "" },
  weekend:    { "stay": 3, "price": "" },
};
Reservation.user = {
	id: "",
}



// so the jquery-rest actions on the back-end know what to do
$.ajaxSetup({
  beforeSend: function(xhr) {
    xhr.setRequestHeader("Accept", "text/javascript");
  }
});



// auto-close the (zurb foundation) alert boxes
$(function(){

  var clearAlert = setTimeout(function(){
    $(".alert-box").fadeOut('slow')
  }, 7000);

  $(document).on("click", ".alert-box a.close", function(event){
    clearTimeout(clearAlert);
  });

  $(document).on("click", ".alert-box a.close", function(event) {
    event.preventDefault();
    $(this).closest(".alert-box").fadeOut(function(event){
      $(this).remove();
    });
  });

});