# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(255)
#  last_name              :string(255)
#  gapps_id               :string(255)
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class User < ActiveRecord::Base
  # Others available are:
  # 	:token_authenticatable, :confirmable, :lockable, 
  # 	:timeoutable, :omniauthable
  devise :database_authenticatable, :registerable, 
         :confirmable, :recoverable, :rememberable, :trackable, 
         :validatable, :email_regexp => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
  				        :first_name, :last_name, :gapps_id

  validate :first_name, :last_name, :presence => true


  # display name for the activeadmin interface
  def display_name
  	"#{self.first_name} #{self.last_name}"
  end

end
