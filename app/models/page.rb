# == Schema Information
#
# Table name: pages
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  body             :text
#  title            :string(255)
#  meta_keywords    :string(255)
#  meta_description :string(255)
#  slug             :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Page < ActiveRecord::Base
  include Rails.application.routes.url_helpers # get _path helpers to work

  before_validation :check_slug

  attr_accessible :body, 
                  :meta_description, 
                  :meta_keywords, 
                  :slug,
                  :name, 
                  :title, 
                  :assets_attributes,
                  :asset

  has_many :assets, :dependent => :destroy

  accepts_nested_attributes_for :assets, :allow_destroy => true


  def check_slug
    self.slug = name if slug.blank?
    self.slug = name.strip.downcase.tr_s('^[a-z0-9]', '-')
  end
end
