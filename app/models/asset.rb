# == Schema Information
#
# Table name: assets
#
#  id         :integer          not null, primary key
#  page_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  image      :string(255)
#  caption    :string(255)
#

# TODO: adapt this to the way the rich editor works
# 		for now, use the file path to generate urls in the gallery view.
class Asset < ActiveRecord::Base
  attr_accessible :page_id, :image, :caption

  belongs_to :page
end
