class ReservationObserver < ActiveRecord::Observer
	def after_update(reservation)
		current_user = User.find reservation.user_id
		# send the payment confirmation email if the booking is confirmed
		ReservationMailer.reservation_confirm(current_user, reservation).deliver if reservation.confirmed
	end
end
