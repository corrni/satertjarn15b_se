# == Schema Information
#
# Table name: rent_periods
#
#  id             :integer          not null, primary key
#  start          :date
#  expires        :date
#  amt_week       :integer
#  amt_short_week :integer
#  amt_weekend    :integer
#  enabled        :boolean
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class RentPeriod < ActiveRecord::Base
  attr_accessible :amt_short_week, :amt_week, :amt_weekend, :enabled, :expires, :start

  has_one :reservation



  def title
  	"#{self.start.to_date} / #{self.expires.to_date}"
  end

  # return applicable rent period
  # TODO: returning the zeroth element of the return array is quite ugly
  # 	  it should be done better
  def self.get_rent_period(date)
  	RentPeriod.where('expires >= ?', date).order('expires ASC').limit(1)[0]
  end

end