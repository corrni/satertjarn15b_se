#encoding:utf-8

# == Schema Information
#
# Table name: reservations
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  rent_period_id   :integer
#  arrival          :date
#  departure        :date
#  confirmed        :boolean
#  reference        :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  price            :integer
#  reservation_type :string(255)
#

class Reservation < ActiveRecord::Base
  attr_accessible :arrival, :departure, 
                  :confirmed, 
                  :reference, 
                  :rent_period_id, :user_id, 
                  :reservation_type,
                  :price

  belongs_to :rent_period
  belongs_to :user


  validates :rent_period_id, :price, :arrival, :departure, :user_id, :presence => true
  validate :reservation_is_available


  # required by the ReservationsController#index for the fullCalendar app
  scope :before, lambda {|end_time| {:conditions => ["departure < ?", Reservation.format_date(end_time)] }}
  scope :after, lambda {|start_time| {:conditions => ["arrival > ?", Reservation.format_date(start_time)] }}



  # NOTE: scopes for the admin interface
  scope :confirmed, where(:confirmed => true)
  


  # (when creating a booking) create a reference number that the customer
  # uses when paying for the reservation (Postgirot message)
  def self.create_ref_string
    t = Time.now
    "#{t.year}-#{t.month}-#{t.day}-#{t.to_i.to_s(36)}".upcase
  end


  # check availability of the requested reservation
  # TODO: localization / i18n (sv/en/..)
  def reservation_is_available
    guest = self.user

    start_date = self.arrival
    end_date = self.departure


    # TODO: at the moment, reservations are fixed, updates will be implemented at a later date
    # 
    # the user is able to change his own reservations, so those aren't
    # # returned
    # reservations = Reservation.where("user_id <> ?", guest).scoped

    reservations = Reservation.scoped


    # guests can't book items in the past
    if (self.arrival < Date.today) || (self.arrival < Date.today)
      errors.add(:arrival, "you're trying to book something in the past!")
    end


    # check for existing reservations
    if ( !reservations.where( { :arrival => (start_date.to_date)..(end_date.to_date)}).blank? )   || 
       ( !reservations.where( { :departure => (start_date.to_date)..(end_date.to_date)}).blank? )

      # NOTE: for simplicity's sake, the rule only checks the arrival date for now
      #       the date range checked here is
      #         [:arrival]    a day later than the requested arrival date (the previous guest departs that day, so the cabin will be available)
      #         [:departure]  a day earlier than the requested departure date
      errors.add(:arrival, "is not available for booking") unless reservations.where({ :arrival => (start_date.to_date + 1.day)..(end_date.to_date - 1.day) })
    end

  end


  def description
  	guest = self.user
  	"#{guest.first_name} #{guest.last_name}"
  end

  
  def self.format_date(date_time)
    Time.at(date_time.to_i).to_formatted_s(:db)
  end

  
end
