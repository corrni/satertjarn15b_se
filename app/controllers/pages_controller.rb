class PagesController < ApplicationController

	def show
		# get the page (or nil, if there's none there by that slug name)
		params[:slug] ||= 'home'
		@page = Page.find_by_slug(params[:slug])
		not_found if @page.nil?

		render :action => 'show'
	end


	# TODO: enable simpleviewer (or other...) galleries to be shown on multiple pages
	def gallery
		home_page = Page.find_by_slug "home"
		@assets = home_page.assets

		render :template => "pages/gallery.xml.builder"		
	end
end