class ReservationsController < ApplicationController
  before_filter :authenticate_user!
  

  

  def index
    # full_calendar will hit the index method with query parameters
    # 'start' and 'end' in order to filter the results for the
    # appropriate month/week/day.  It should be possible to change
    # this to be starts_at and ends_at to match rails conventions.

    @reservations = Reservation.scoped

    # TODO: only get the current user's reservations (if he has any...)
    # @reservations = @reservations.where('user_id = ?', current_user.id)


    @reservations = @reservations.after(params['start']) if (params['start'])
    @reservations = @reservations.before(params['end']) if (params['end'])

    respond_to do |format|
      format.html
      format.json
    end
  end



  def show
    @reservation = Reservation.find(params[:id])
      
    respond_to do |format|
      format.html # show.html.erb
      format.json {}
    end
  end



  def new
    @reservation = Reservation.new
    @reservation.rent_period = RentPeriod.get_rent_period(params[:arrival].to_date)

    respond_to do |format|
      format.html # new.html.erb
      format.json {}
    end
  end


  def edit
    @reservation = Reservation.find(params[:id])
  end



  def create
    @reservation = Reservation.new(params[:reservation])

    # an administrator needs to confirm reservations
    @reservation.confirmed = false
    @reservation.reference = Reservation.create_ref_string

    

    respond_to do |format|
      if @reservation.save
        ReservationMailer.reservation_create(current_user, @reservation).deliver
        ReservationMailer.reservation_admin_notification(current_user, @reservation).deliver
        format.html { redirect_to @reservation, notice: 'Reservation was successfully created.' }
        format.json { render json: @reservation, status: :created, location: @reservation }
      else
        format.html { render action: "new" }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end



  # when we drag an event on the calendar (from day to day on the month view, or stretching
  # it on the week or day view), this method will be called to update the values.
  # viva la REST!
  def update
    @reservation = Reservation.find(params[:id])

    respond_to do |format|
      if @reservation.update_attributes(params[:reservation])
        
        
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        # format.json { head :no_content }
        format.json { render json: @reservation }
      else
        format.html { render action: "edit" }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end




  def destroy
    @reservation = Reservation.find(params[:id])
    @reservation.destroy

    respond_to do |format|
      format.html { redirect_to reservations_url }
      format.json { head :ok }
    end
  end
end



