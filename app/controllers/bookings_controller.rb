class BookingsController < ApplicationController

	before_filter :authenticate_user!

	def index
		@bookings = Reservation.where(:user_id => current_user).size
	end
end