#encoding:utf-8
class ReservationMailer < ActionMailer::Base
  layout 'reservation_mailer'

  default from: "support@satertjarn15b.se"

  def reservation_create(user, reservation)
  	@user = user
  	@reservation = reservation
  	mail(:to => user.email, :subject => "Tack för din bokning")
  end



  def reservation_confirm(user, reservation)
  	@user = user
  	@reservation = reservation
  	mail(:to => user.email, :subject => "Bekräftelse: din betalning till satertjarn15b.se har mottagits")
  end

  def reservation_admin_notification(user, reservation)
    @user = user
    @reservation = reservation

    mail(:to => "info@satertjarn15b.se", :subject => "Ny bokning (ref nr. #{reservation.reference})")
  end


end
