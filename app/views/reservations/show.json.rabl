object @reservation


node(:url) 			{ "" }
node(:title)		{ |res| res.reference }
node(:description)  { |res| res.reference }


attributes :id, :arrival, :departure, :reference,  :reservation_type, :price, :confirmed

node(:amt_week) 		{ |res| res.rent_period.amt_week }
node(:amt_short_week)   { |res| res.rent_period.amt_short_week }
node(:amt_weekend) 		{ |res| res.rent_period.amt_weekend }
node(:rent_period_id)	{ |res| res.rent_period.id }

node(:user_id)			{ |res| current_user.id }
