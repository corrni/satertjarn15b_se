# TODO: do you need these?
# imagePath="images/",
# thumbPath="thumbs/"

xml.instruct!

xml.simpleviewergallery("galleryStyle" 			=> "MODERN",
						"title"					=> "",
						"textColor" 			=> "FFFFFF",
						"frameColor" 			=> "FFFFFF",
						"frameWidth"			=> "5",
						"thumbPosition" 		=> "LEFT",
						"thumbColumns"			=> "2",
						"thumbRows"				=> "3",
						"showOpenButton" 		=> "FALSE",
						"showFullscreenButton" 	=> "FALSE",
						"maxImageWidth" 		=> "800",
						"maxImageHeight"		=> "800",
						"useFlickr" 			=> "false",
						"flickrUserName"		=> "",
						"flickrTags"			=>"",
						"languageCode"			=>"AUTO",
						"languageList"			=>"") do
	@assets.each do |asset|
		thumb_url = "#{request.protocol}#{request.host_with_port.sub(/:80$/,"")}/#{asset.image.sub(/^\//,"")}"
		main_url = thumb_url.sub(/thumb/, 'main')
		xml.image "imageURL" => main_url,
			  	  "thumbURL" => thumb_url do
			  	  	xml.caption asset.caption
			  	  end
	end
end


