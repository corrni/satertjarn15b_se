ActiveAdmin.register Page do

  config.comments = false

  filter :name
  filter :body

  index do
    # column  "Path", :slug
    column "Page Name" do |page|
      link_to page.name, admin_page_path(page)
    end
    column :body
        
    default_actions
  end

  show do |page|
    attributes_table do
      row :name
      row :meta_keywords
      row :meta_description
      row :body

      # TODO: show a thumbnail of the images
    end
  end


  form :html => { :multipart => true } do |f|
  	f.inputs "Basic Information" do
  		f.input :name
  		f.input :title
  	end

    f.inputs "Page Text" do
     f.input :body, :label => false, :as => :rich, :config => {:width => '76%', :height => '400px'}
   end 

   # TODO: - collapsible sections (especially for something like a pic gallery)
   #       - grid view for active_admin forms?
   # FIXME: hide the image hint when there's no image
   f.inputs "Gallery" do
      f.has_many :assets do |p|
      	p.input :caption
      	p.input :image, 
      			:as => :rich_picker, 
            :label => "Photo",
      			:config => { :style => 'width: 200px !important;', :scoped => "simpleviewer_home" }

        # TODO: find a sexier way of doing this
        p.input :_destroy, :as => :boolean, :required => false, :label=> 'Delete Photo'
      end
   end

  

   f.inputs "Metadata" do
      f.input :meta_keywords
      f.input :meta_description
   end

   f.buttons

  end

end


