# encoding:utf-8;
ActiveAdmin.register Reservation do
	menu :parent => "Bokningar"


	scope :all, :default => true
	scope :confirmed


	filter :user, :label => "Gäst"
	filter :arrival
	filter :departure
	filter :reference




	index do
		selectable_column
		default_actions
		
		column "Gäst", 	  :user
		column "Ankomst", :arrival
		column "Aversa",  :departure
		
		column "Pris", :price do |r|
			number_to_currency r.price
		end

		
	end

  
end
