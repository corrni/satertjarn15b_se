ActiveAdmin.register RentPeriod do
  menu :parent => "Bokningar"

  filter :start
  filter :expires

  batch_action :enable do |selection|
  	RentPeriod.find(selection).each {|i| i.enabled = true}
  end

  index do
  	selectable_column
  	column "Start Datum", :start
  	column "Slut Datum", :expires

  	column "Pris / Hel Vecka", :amt_week do |r|
      number_to_currency r.amt_week
    end

  	column "Pris / Kortvecka", :amt_short_week, sortable: :amt_short_week do |r|
      number_to_currency r.amt_short_week
    end
  	
    column "Pris / Weekend", :amt_weekend do |r|
      number_to_currency r.amt_weekend
    end

  	column "Aktiv", :enabled

  	default_actions
  end

  form do |f|
    f.inputs "Start/Slut Datum" do
      f.input :start, :as => :datepicker
      f.input :expires, :as => :datepicker
    end

    f.inputs "Priser" do
      f.input :amt_week, :label => "Vecka"
      f.input :amt_short_week, :label => "Kortvecka"
      f.input :amt_weekend, :label => "Weekend"
    end

    f.inputs "" do
      f.input :enabled, :label => "Aktiverat?"
    end

    f.buttons
  end
end
